package main

import (
    "fmt"
    "strings"
    "sync"
    "sync/atomic"
    "time"
)

const threads = 12

var board [size][size]byte
var boardIndex int32
var done sync.WaitGroup
var searchedBuffer[threads][size][size]bool

func createBoard() {
    start := time.Now()

    var builder strings.Builder

    for i := 0; i < size; i++ {
        for j := 0; j < size; j++ {
            r := nextByte()
            board[i][j] = r
            if size <= 25 {
                builder.WriteByte(r + chCapitalOffset)
                builder.WriteByte(32)
            } else if size <= 50 {
                builder.WriteByte(r + chCapitalOffset)
            }
        }
        if size <= 50 {
            builder.WriteByte(10)
        }
    }

    fmt.Printf("Created a %d by %d board in %v!\n", size, size, time.Now().Sub(start))
    fmt.Print(builder.String())
}

func search() {
    done.Add(threads)

    for i := 0; i < threads; i++ {
        id := i
        go func() {
            buffer := &searchedBuffer[id]
            rootContents := root.contents
            for {
                index := atomic.AddInt32(&boardIndex, 1) - 1
                if index < size * size {
                    x := int(index) % size
                    y := int(index) / size
                    find(rootContents[board[x][y]], buffer, x, y)
                } else {
                    break
                }
            }

            done.Done()
        }()
    }

    done.Wait()
}

func find(p *Node, searched *[size][size]bool, x int, y int) bool {
    if p.data & closedBit == wordBit { // Is word that hasn't been closed
        if p.data == wordBit { // Is empty
            p.data |= deletedFoundBit
            return true
        } else {
            p.data |= foundBit
        }
    }

    searched[x][y] = true

    px := x + 1
    mx := x - 1
    py := y + 1
    my := y - 1
    var q *Node

    if mx >= 0 && !searched[mx][y] {
        if q = p.contents[board[mx][y]]; q != nil && q.data < deletedBit && find(q, searched, mx, y) {
            p.data--
            if p.data & refBits == 0 {
                p.data |= deletedBit
                searched[x][y] = false
                return true
            }
        }
    }
    if my >= 0 && !searched[x][my] {
        if q = p.contents[board[x][my]]; q != nil && q.data < deletedBit && find(q, searched, x, my) {
            p.data--
            if p.data & refBits == 0 {
                p.data |= deletedBit
                searched[x][y] = false
                return true
            }
        }
    }
    if mx >= 0 && my >= 0 && !searched[mx][my] {
        if q = p.contents[board[mx][my]]; q != nil && q.data < deletedBit && find(q, searched, mx, my) {
            p.data--
            if p.data & refBits == 0 {
                p.data |= deletedBit
                searched[x][y] = false
                return true
            }
        }
    }
    if px < size && !searched[px][y] {
        if q = p.contents[board[px][y]]; q != nil && q.data < deletedBit && find(q, searched, px, y) {
            p.data--
            if p.data & refBits == 0 {
                p.data |= deletedBit
                searched[x][y] = false
                return true
            }
        }
    }
    if my >= 0 && px < size && !searched[px][my] {
        if q = p.contents[board[px][my]]; q != nil && q.data < deletedBit && find(q, searched, px, my) {
            p.data--
            if p.data & refBits == 0 {
                p.data |= deletedBit
                searched[x][y] = false
                return true
            }
        }
    }
    if py < size && !searched[x][py] {
        if q = p.contents[board[x][py]]; q != nil && q.data < deletedBit && find(q, searched, x, py) {
            p.data--
            if p.data & refBits == 0 {
                p.data |= deletedBit
                searched[x][y] = false
                return true
            }
        }
    }
    if mx >= 0 && py < size && !searched[mx][py] {
        if q = p.contents[board[mx][py]]; q != nil && q.data < deletedBit && find(q, searched, mx, py) {
            p.data--
            if p.data & refBits == 0 {
                p.data |= deletedBit
                searched[x][y] = false
                return true
            }
        }
    }
    if px < size && py < size && !searched[px][py] {
        if q = p.contents[board[px][py]]; q != nil && q.data < deletedBit && find(q, searched, px, py) {
            p.data--
            if p.data & refBits == 0 {
                p.data |= deletedBit
                searched[x][y] = false
                return true
            }
        }
    }
    searched[x][y] = false
    return false
}

// Iterative find
func find2(searched *[size][size]bool, x, y int) {
    type State struct {
        x, y, index int
        node *Node
    }

    var stack [longestWord]State
    var sp uint

    stack[0] = State{x: x, y: y, node: root.contents[board[x][y]]}

    if p := stack[0].node; p.data & closedBit == wordBit {
        p.data = p.data | foundBit
    }

loop:
    // Pop stack
    if sp < longestWord {
        state := stack[sp]
        p := state.node
        index := state.index
        x = state.x
        y = state.y

        var qx int
        var qy int
    loop2:
        for {
            switch index {
            case 0:
                if x - 1 > 0 && !searched[x - 1][y] {
                    qx = x - 1
                    qy = y
                    break loop2
                }
            case 1:
                if y - 1 > 0 && !searched[x][y - 1] {
                    qx = x
                    qy = y - 1
                    break loop2
                }
            case 2:
                if x - 1 > 0 && y - 1 > 0 && !searched[x - 1][y - 1] {
                    qx = x - 1
                    qy = y - 1
                    break loop2
                }
            case 3:
                if x + 1 < size && !searched[x + 1][y] {
                    qx = x + 1
                    qy = y
                    break loop2
                }
            case 4:
                if y - 1 > 0 && x + 1 < size && !searched[x + 1][y - 1] {
                    qx = x + 1
                    qy = y - 1
                    break loop2
                }
            case 5:
                if y + 1 < size && !searched[x][y + 1] {
                    qx = x
                    qy = y + 1
                    break loop2
                }
            case 6:
                if x - 1 > 0 && y + 1 < size && !searched[x - 1][y + 1] {
                    qx = x - 1
                    qy = y + 1
                    break loop2
                }
            case 7:
                if x + 1 < size && y + 1 < size && !searched[x + 1][y + 1] {
                    qx = x + 1
                    qy = y + 1
                    break loop2
                }
            default:
                sp--
                searched[x][y] = false
                goto loop
            }

            index++
        }


        q := p.contents[board[qx][qy]]
        index++

        if q == nil || q.data >= deletedBit {
            goto loop2
        } else if q.data & closedBit == wordBit { // Is word that hasn't been closed
            if q.data == wordBit { // Is empty
                q.data = q.data | deletedFoundBit

            del:
                p.data--
                if p.data & refBits == 0 {
                    p.data = p.data | deletedBit
                    searched[x][y] = false

                    // Pop stack
                    sp--
                    if sp >= longestWord { // Almost never happens (only when found every word)
                        return
                    }
                    state := stack[sp]
                    p = state.node
                    index = state.index
                    x = state.x
                    y = state.y

                    goto del
                } else {
                    goto loop2
                }
            } else {
                q.data = q.data | foundBit
            }
        }

        searched[x][y] = true

        // Push stack
        stack[sp] = State{node: p, index: index, x: x, y: y}
        sp++

        p = q
        index = 0
        x = qx
        y = qy

        goto loop2
    } else {
        return
    }
}
