package main

import (
    "math/rand"
    "time"
)

const randomSeed = true

var (
    random *rand.Rand
    alias [chMax]int
    prob  [chMax]float64
)

func init() {
    seed := int64(1)
    if randomSeed {
        seed = int64(time.Now().Nanosecond())
    }

    random = rand.New(rand.NewSource(seed))
}

func loadWeights(weights [chMax]float64) {
    // https://www.keithschwarz.com/darts-dice-coins/

    const n = len(weights)

    var (
        small []int
        large []int
        g int
        l int
    )

    for i, w := range weights {
        w *= float64(n)
        weights[i] = w
        if w < 1 {
            small = append(small, i)
        } else {
            large = append(large, i)
        }
    }

    for len(small) > 0 && len(large) > 0 {
        l, small = popIndex(small)

        g, large = popIndex(large)

        prob[l] = weights[l]
        alias[l] = g

        pg := (weights[g] + weights[l]) - 1
        weights[g] = pg
        if pg < 1 {
            small = append(small, g)
        } else {
            large = append(large, g)
        }
    }

    for len(large) > 0 {
        g, large = popIndex(large)

        prob[g] = 1
    }

    for len(small) > 0 {
        g, small = popIndex(small)

        prob[g] = 1
    }
}

func popIndex(slice []int) (int, []int) {
    val := slice[0]

    if len(slice) > 1 {
        slice[0] = slice[len(slice) - 1]
    }
    slice = slice[:len(slice) - 1]

    return val, slice
}

// Returns 0-25 representing a-z
func nextByte() byte {
    i := int(random.Float64() * chMax)

    if random.Float64() < prob[i] {
        return byte(i)
    }
    return byte(alias[i])
}