package main

import (
    "flag"
    "fmt"
    "log"
    "os"
    "runtime"
    "runtime/pprof"
    "time"
)

const size = 50
const maxOutput = 1000

func main() {
    var cpuProfile = flag.String("cpuprofile", "", "write cpu profile to `file`")
    var memProfile = flag.String("memprofile", "", "write memory profile to `file`")

    flag.Parse()
    if *cpuProfile != "" {
        f, err := os.Create(*cpuProfile)
        if err != nil {
            log.Fatal("Could not create CPU profile: ", err)
        }
        defer f.Close()

        if err := pprof.StartCPUProfile(f); err != nil {
            log.Fatal("Could not start CPU profile: ", err)
        }
        defer pprof.StopCPUProfile()
    }

    run()

    if *memProfile != "" {
        f, err := os.Create(*memProfile)
        if err != nil {
            log.Fatal("Could not create memory profile: ", err)
        }
        defer f.Close()

        runtime.GC()

        if err := pprof.WriteHeapProfile(f); err != nil {
            log.Fatal("Could not write memory profile: ", err)
        }
    }
}

func run() {
    load()
    createBoard()
    start := time.Now()
    search()
    total := time.Now().Sub(start)
    printFound()
    fmt.Printf("Search took %v.\n", total)
}
